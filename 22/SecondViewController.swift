//
//  SecondViewController.swift
//  22
//
//  Created by Mac on 15/09/2019.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var labelOnSecondVC: UILabel!
    var labelColour: UIColor = .black
    
    override func viewDidLoad() {
        labelOnSecondVC.backgroundColor = labelColour
        super.viewDidLoad()
        }
    
    func reColor(color:UIColor) {
        self.labelOnSecondVC.backgroundColor = color
    }
    
    
    }
    
    
    
    
    




