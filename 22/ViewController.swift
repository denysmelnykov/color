import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var hiLabel: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func green(_ sender: Any) {
        self.hiLabel.backgroundColor = .green        
    }
    
    
    @IBAction func red(_ sender: Any) {
        self.hiLabel.backgroundColor = .red
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let destinationVC: SecondViewController = segue.destination as! SecondViewController
        destinationVC.labelColour = self.hiLabel.backgroundColor!
    }

    
    
}

    

    
    
    

    



